# @field DEVICENAME
# @type STRING
# Device name, should be the same as the name in CCDB

# @field CONTROLLERNAME
# @type STRING
# Name of the head unit

# @field CHANNEL
# @type STRING
# Channel on the head unit where the mass flow controller is connected to (A1, A2, B1, B2, C1, C2)

# @field RELAY1
# @type INTEGER
# Number of the first relay mapped to this gauge

# @field RELAY2
# @type INTEGER
# Number of the second relay mapped to this gauge

dbLoadRecords(vac_mfc_mks_gv50a.db, "P = $(DEVICENAME), R = :, CONTROLLERNAME = $(CONTROLLERNAME), ASYNPORT = $(CONTROLLERNAME), CHANNEL=$(CHANNEL), RELAY1=$(RELAY1), RELAY2=$(RELAY2)")

dbLoadRecords(vac_ctrl_mks946_937b_gauge_relay.db, "R = :, CONTROLLERNAME = $(CONTROLLERNAME), GAUGE = $(DEVICENAME), RELAY = $(RELAY1), n = 1")
dbLoadRecords(vac_ctrl_mks946_937b_gauge_relay.db, "R = :, CONTROLLERNAME = $(CONTROLLERNAME), GAUGE = $(DEVICENAME), RELAY = $(RELAY2), n = 2")

seq check_flow_rate("P = $(DEVICENAME), R = :, name = $(DEVICENAME)")
