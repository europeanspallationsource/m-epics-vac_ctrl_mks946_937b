# Vacuum Mass Flow & Gauge Controller MKS 946, Vacuum Gauge Controller MKS 937B
# Vacuum Cold Cathode Gauge MKS
# Vacuum Capacitance Gauge MKS
# Vacuum Pirani Gauge MKS
# Mass Flow Controller MKS GV50A

EPICS module to provide communications and read/write data from/to MKS 946 vacuum mass flow & gauge controller and MKS 937B vacuum gauge controller and
*   to read/write data for MKS vacuum cold cathode gauge.
*   to read/write data for MKS vacuum capacitance gauge.
*   to read/write data for MKS vacuum pirani gauge.
*   to read/write data for MKS GV50A mass flow controller (MFC).

IOC does not directly communicate with the MFC, the gauge is actully connected to a vacuum controller. IOC communicated to the controller and controller provides MFC data. Controllers can have multiple gauges and MFCs in different channels.

The modules arrangement should be something like:
```
IOC-|
    |-vac-ctrl-mks946_937b
    |	   |-vac-gauge-mks-vgp (Pirani gauge)
    |	   |-vac-gauge-mks-vgp (Pirani gauge)
    |      |-vac-gauge-mks-vgc (Cold cathode gauge)
    |
    |-vac-ctrl-mks946_937b
    |	   |-vac-gauge-mks-vgp (Pirani gauge)
    |	   |-vac-mfc-mks-gv50a (Mass flow controller)
```

This allows EPICS modules for gauges to be re-used and any combination of controller and gauges to be built from existing modules.

## Startup Examples

`iocsh -r vac_ctrl_mks946_937b,6.0.0 -c 'requireSnippet(vac_ctrl_mks946_937b_ethernet.cmd, "DEVICENAME=LEBT-010:VAC-VEVMC-01100, IPADDR=10.4.0.213, PORT=4004")' -c 'requireSnippet(vac_gauge_mks_vgc.cmd, "DEVICENAME=LEBT-010:VAC-VGC-10000, CONTROLLERNAME=LEBT-010:VAC-VEVMC-01100, CHANNEL=A1")' -c 'requireSnippet(vac_gauge_mks_vgd.cmd, "DEVICENAME=LEBT-010:VAC-VGD-10000, CONTROLLERNAME=LEBT-010:VAC-VEVMC-01100, CHANNEL=B1")' -c 'requireSnippet(vac_gauge_mks_vgp.cmd, "DEVICENAME=LEBT-010:VAC-VGP-10000, CONTROLLERNAME=LEBT-010:VAC-VEVMC-01100, CHANNEL=C1") -c 'requireSnippet(vac_mfc_mks_gv50a.cmd, "DEVICENAME=LEBT-010:VAC-VVMC-01100, CONTROLLERNAME=LEBT-010:VAC-VEVMC-01100, CHANNEL=A2")'`

If ran as proper ioc service:
```
require vac_ctrl_mks946_937b, 6.0.0

epicsEnvSet(DEVICENAME, "LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(IPADDR, "10.10.1.21")
epicsEnvSet(PORT, "4004")
< ${REQUIRE_vac_ctrl_mks946_937b_PATH}/startup/vac_ctrl_mks946_937b_ethernet.cmd

epicsEnvSet(DEVICENAME, "LEBT-010:VAC-VGC-10000")
epicsEnvSet(CONTROLLERNAME, "LEBT-010:VAC-VEG-10010")
epicsEnvSet(CHANNEL, "A1")
< ${REQUIRE_vac_ctrl_mks946_937b_PATH}/startup/vac_gauge_mks_vgc.cmd

epicsEnvSet(DEVICENAME, "LEBT-010:VAC-VGD-10000")
epicsEnvSet(CONTROLLERNAME, "LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(CHANNEL, "B1")
< ${REQUIRE_vac_ctrl_mks946_937b_PATH}/startup/vac_gauge_mks_vgd.cmd

epicsEnvSet(DEVICENAME, "LEBT-010:VAC-VGP-00031")
epicsEnvSet(CONTROLLERNAME, "LEBT-010:VAC-VEG-10010")
epicsEnvSet(CHANNEL, "C1")
< ${REQUIRE_vac_ctrl_mks946_937b_PATH}/startup/vac_gauge_mks_vgp.cmd

epicsEnvSet(DEVICENAME, "LEBT-010:VAC-VVMC-04100")
epicsEnvSet(CONTROLLERNAME, "LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(CHANNEL, "A2")
< ${REQUIRE_vac_ctrl_mks946_937b_PATH}/startup/vac_mfc_mks_gv50a.cmd
```
