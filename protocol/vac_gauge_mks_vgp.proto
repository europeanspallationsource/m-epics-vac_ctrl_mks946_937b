################################################################################
# Protocol file for pirani gauge for
# MKS946 Vacuum Mass Flow & Gauge Controller or
# MKS937B Gauge Controller
################################################################################

Terminator  = ";FF";

ReadTimeout   = 5000;
WriteTimeout  = 5000;
PollPeriod    = 100;
ReplyTimeout  = 5000;
LockTimeout   = 30000;

ExtraInput    = Ignore;

start_comm    = "@254";
ack_resp      = "@%*dACK";
nak_resp      = "@%*dNAK";
start_comm    = "@%(\$1)03d";
ack_resp      = "@%(\$1)=03dACK";
nak_resp      = "@%(\$1)=03dNAK";

channel       = "%(\$2)d";
on_off        = "%{OFF|ON}";

reconnect {
    disconnect;
    wait 1000;
    connect 1000;
}
@replytimeout {
    reconnect;
}

################################################################################
################################################################################
#
# Pirani
#
################################################################################
################################################################################

################################################################################
# Get previously set atmosphreic pressure (for ATM calibration)
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_atm_calibration {
    out $start_comm "ATM" $channel "?";
    in  $ack_resp   "%f";

    @mismatch {
        in $nak_resp "156";
    }
}

################################################################################
# Send atmosphreic pressure to perform ATM calibration
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_atm_calibration {
    out $start_comm "ATM" $channel "!%f";
    in  $ack_resp   "%f";
}

################################################################################
# Zero the Pirani gauge
# Protocol arguments:
#  1. Serial address
#  2. Channel
#  3. Result PV
################################################################################
set_zero {
    out $start_comm "VAC" $channel "!";
    in  $ack_resp   "%(\$3){NAK|OK}";
}

################################################################################
# Get the autozero channel
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
az_channels = "%{NA|A1|A2|B1|B2|C1|C2}";
get_autozero_channel {
    out $start_comm "AZ" $channel "?";
    in  $ack_resp   $az_channels;

    @mismatch {
        in $nak_resp "156";
    }
}

################################################################################
# Set the autozero channel
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_autozero_channel {
    out $start_comm "AZ" $channel "!" $az_channels;
    in  $ack_resp                     $az_channels;

    @mismatch {
        in $nak_resp "156";
    }
}

################################################################################
# Get gas type
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
gas_types = "%{NITROGEN|ARGON|HELIUM}";
get_gas_type {
    out $start_comm "GT" $channel "?";
    in  $ack_resp   $gas_types;
}

################################################################################
# Set gas type
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_gas_type {
    out $start_comm "GT" $channel "!" $gas_types;
    in  $ack_resp                     $gas_types;
}

################################################################################
# Get power status
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_power_status {
    out $start_comm "CP" $channel "?";
    in  $ack_resp   $on_off;
}

################################################################################
# Set power status
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_power_status {
    out $start_comm "CP" $channel "!" $on_off;
    in  $ack_resp                     $on_off;
}

################################################################################
# Reset to factory default
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
reset_to_factory_default {
    out $start_comm "FD" $channel "!";
    in  $ack_resp   "%{OK}";
}
